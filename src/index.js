import express from "express";
import { searchRestaurants, singleRestaurants } from "./open-table/restaurants";
import dotEnv from "dotenv";

const app = express();
dotEnv.config();

app.get("/search-restaurants", searchRestaurants);
app.get("/single-restaurant/:id", singleRestaurants);

app.listen(process.env.APP_PORT, () => {
  console.log(`App is running on ${process.env.APP_PORT} port `);
});
