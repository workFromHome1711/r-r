import axios from "axios";

export const searchRestaurants = async (req, res) => {
  const { query } = req;

  axios
    .get(process.env.SEARCH_RESTAURANTS, { params: query })
    .then((response) => {
      res.send(response.data);
    })
    .catch((error) => {
      res.status(error.response.status).send(error);
    });
};

export const singleRestaurants = async (req, res) => {
  const { params } = req;

  if (Object.keys(params).length === 0) {
    res.status(400).send({
      message: "Please provide id",
    });
  }

  axios
    .get(`${process.env.SEARCH_RESTAURANTS}/${params.id}`)
    .then((response) => {
      res.send(response.data);
    })
    .catch((error) => {
      if (!error.response) {
        res.status(400).send("BAD REQUEST");
      }
      res.status(error.response.status).send(error);
    });
};
